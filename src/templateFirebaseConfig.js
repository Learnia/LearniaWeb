// Cuando se coloquen los datos, renombrar el archivo a
// firebaseConfig.js

export const config = {
  apiKey: '',
  authDomain: '',
  databaseURL: '',
  projectId: '',
  storageBucket: '',
  messagingSenderId: ''
}

export function getConfig () {
  return config
}
