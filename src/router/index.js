import Vue from 'vue'
import Router from 'vue-router'

import LandingPage from '@/components/Pages/LandingPage'
import Inside from '@/components/Pages/Inside'
// import Test from '@/components/Test'

Vue.use(Router)

let router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Inside',
      component: Inside
      // meta: { requiresAuth: true }
    },
    {
      path: '/start',
      name: 'LandingPage',
      component: LandingPage
    }
  ]
})

/* router.beforeEach((to, from, next) => {
  if (to.meta.requiresAuth) {
    next()
  } else {
    next({ name: 'LandingPage' })
  }
}) */
/*
router.beforeEach((to, from, next) => {
  // check for auth
  console.log('check auth', to, from, this, router)

  next()
}) */

export default router
